﻿package com.example.a005977513csusbedu.currency;

//package com.example.a005977513.currency;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends AppCompatActivity {
    //initilaize the variables
    private EditText editText1;
    private Button  convbutton;
    private TextView textView1;
    private String usd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText1 = findViewById(R.id.EditText01);

        convbutton = findViewById(R.id.bnt);

        textView1 = findViewById(R.id.Yen);

        convbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen){
                usd = editText1.getText().toString();

                if(usd.equals("")){
                    textView1.setText("This Cannot be blank");
                }
                else{
                    Double dInp = Double.parseDouble(usd);
                    Double resul = dInp*112.57;

                    textView1.setText("$" + usd + " = " + "¥"+String.format("%.2f", resul));
                    editText1.setText("");



                }
            }
        });

    }
}

