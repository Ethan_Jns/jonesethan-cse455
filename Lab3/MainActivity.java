﻿//package currencycalculator.currencycalculator;

//package com.example.a005977513csusbedu.currency;

package com.example.a005977513.currency;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;


public class MainActivity extends AppCompatActivity {
    //initilaize the variables
    private EditText editText1;
    private Button  convbutton;
    private TextView textView1;
    private String usd;
    private String url = "https://api.fixer.io/latest?base=USD";
    String json = ""; //Buffered JSON
    String line = ""; //Will impliment readLine()
    String rate = ""; //Will obtain value


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Cast variables to their id's
        editText1 = findViewById(R.id.EditText01);
        convbutton = findViewById(R.id.bnt);
        textView1 = findViewById(R.id.Yen);
      //  BackgroundTask object = new BackgroundTask();

        convbutton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View convertToYen) {
                System.out.println("\nTesting 1...Before Asynch");
                BackgroundTask object = new BackgroundTask();
                object.execute();
                System.out.println("\nTesting 2...After Asynch");

            }
            //}
        });
    }

    private class BackgroundTask extends AsyncTask<Void, Void, String>{
        @Override
        protected void onPreExecute(){super.onPreExecute();}
        @Override
        protected String doInBackground(Void... params){
            Log.i("test", "this is :: " + rate);

            try{
                URL web_url = new URL(MainActivity.this.url);

                HttpURLConnection httpURLConnection = (HttpURLConnection)web_url.openConnection();

                httpURLConnection.setRequestMethod("GET");

                System.out.println("Test URL method\n");

                httpURLConnection.connect();

                InputStream inputStream = httpURLConnection.getInputStream();

                BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream));

                System.out.println("Succsesful Connection!!\n");
                Log.i("test", "this is :: " + rate);

                while(line != null){
                    line = bufferedReader.readLine();
                    json += line;
                }
                System.out.println("\nThe JSON: " + json);

                JSONObject jso = new JSONObject(json);
                JSONObject jsoRate = jso.getJSONObject("rates");

                rate = jsoRate.get("JPY").toString();
                Log.i("test", "this is :: " + rate);
            }

            catch (IOException e) {
                Log.i("test", "inside IO);");

                e.printStackTrace();
            }
            catch (JSONException e) {
                //Log.e(tag:"MYAPP", msg:"Unexpected JSON exception", e);
                Log.i("test", "inside JSON");

                e.printStackTrace();
            }
           /* catch (MalformedURLException e){
            e.printStackTrace();
            }*/

            return rate;
        }

        @Override
        protected void onPostExecute(String tate){
            super.onPostExecute(tate);
            Double value = Double.parseDouble(tate);
            Log.i("test", "this is :: " + tate);

            usd = editText1.getText().toString();

            if(usd.equals("")){
                textView1.setText("This Cannot be blank");
            }
            else{
                Double dInp = Double.parseDouble(usd);
                Double result = dInp*value;

                textView1.setText("$" + usd + " = " + "¥"+String.format("%.2f", result));
                editText1.setText("");
            }
        }



    }
}






